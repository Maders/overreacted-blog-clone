export interface ApiPost {
  userId: number;
  id: number;
  title: string;
  body: string;
}

export interface Post extends ApiPost {
  createdAt: string;
  timeToRead: number;
  spoiler: string;
}
