import { PUBLIC_BASE_URL } from 'src/core/envs';

export const websiteConfigs = {
  metadata: {
    title: 'Overreacted',
    description: 'Personal blog by Dan Abramov. I explain with words and code.',
    siteUrl: PUBLIC_BASE_URL?.concat('/'),
    social: { twitter: 'https://twitter.com/dan_abramov' },
  },
};
