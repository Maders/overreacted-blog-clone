import Head from 'next/head'
import React from 'react'

function main() {
  return (
    <Head>
        <link
          type="image/png"
          href="/favicon-32x32.png?v=8c19a894ebc3f54d282a8f2418cf5398"
        />
        <link
          rel="icon"
          href="https://overreacted.io/favicon-32x32.png?v=8c19a894ebc3f54d282a8f2418cf5398"
          type="image/png"
        />
        <link
          rel="manifest"
          href="https://overreacted.io/manifest.webmanifest"
          crossOrigin="anonymous"
        />
        <link
          rel="apple-touch-icon"
          sizes="48x48"
          href="https://overreacted.io/icons/icon-48x48.png?v=8c19a894ebc3f54d282a8f2418cf5398"
        />
        <link
          rel="apple-touch-icon"
          sizes="72x72"
          href="https://overreacted.io/icons/icon-72x72.png?v=8c19a894ebc3f54d282a8f2418cf5398"
        />
        <link
          rel="apple-touch-icon"
          sizes="96x96"
          href="https://overreacted.io/icons/icon-96x96.png?v=8c19a894ebc3f54d282a8f2418cf5398"
        />
        <link
          rel="apple-touch-icon"
          sizes="144x144"
          href="https://overreacted.io/icons/icon-144x144.png?v=8c19a894ebc3f54d282a8f2418cf5398"
        />
        <link
          rel="apple-touch-icon"
          sizes="192x192"
          href="https://overreacted.io/icons/icon-192x192.png?v=8c19a894ebc3f54d282a8f2418cf5398"
        />
        <link
          rel="apple-touch-icon"
          sizes="256x256"
          href="https://overreacted.io/icons/icon-256x256.png?v=8c19a894ebc3f54d282a8f2418cf5398"
        />
        <link
          rel="apple-touch-icon"
          sizes="384x384"
          href="https://overreacted.io/icons/icon-384x384.png?v=8c19a894ebc3f54d282a8f2418cf5398"
        />
        <link
          rel="apple-touch-icon"
          sizes="512x512"
          href="https://overreacted.io/icons/icon-512x512.png?v=8c19a894ebc3f54d282a8f2418cf5398"
        />
      </Head>
  )
}

export default main