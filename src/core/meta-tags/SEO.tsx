import React from 'react';
import Head from 'next/head';
import { websiteConfigs } from 'src/core/siteConfig';

interface SEOProps {
  meta?: JSX.IntrinsicElements['meta'][];
  image?: string;
  title?: string;
  description?: string;
  slug?: string;
}

export function SEO(props: SEOProps) {
  const {
    description,
    image,
    meta: additionalMeta,
    slug,
    title: titleProps,
  } = props;

  const title = `${
    titleProps ?? websiteConfigs.metadata.title
  } — A blog by Dan Abramov`;

  const url = slug
    ? websiteConfigs.metadata.siteUrl?.concat(slug)
    : websiteConfigs.metadata.siteUrl;

  let metas: JSX.IntrinsicElements['meta'][] = [
    {
      name: 'description',
      content: description ?? websiteConfigs.metadata.description,
    },
    {
      property: 'og:url',
      content: url,
    },
    {
      property: 'og:title',
      content: title,
    },
    {
      property: 'og:description',
      content: description,
    },
    {
      name: 'twitter:card',
      content: 'summary',
    },
    {
      name: 'twitter:creator',
      content: websiteConfigs.metadata.social.twitter,
    },
    {
      name: 'twitter:title',
      content: title,
    },
    {
      name: 'twitter:description',
      content: description,
    },
  ];

  metas = additionalMeta ? metas.concat(additionalMeta) : metas;
  metas = image
    ? metas.concat([
        {
          property: 'og:image',
          content: image,
        },
        {
          name: 'twitter:image',
          content: image,
        },
      ])
    : metas;

  return (
    <Head>
      <title>{title}</title>
      {metas.map((meta) => (
        <meta key={meta.name || meta.property} {...meta} />
      ))}
    </Head>
  );
}
