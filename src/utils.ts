export function formatReadingTime(minutes: number) {
  const countOfFiveMinuteSlices = Math.round(minutes / 5);
  const isMoreThan25Min = countOfFiveMinuteSlices > 5;

  if (!isMoreThan25Min)
    return `${new Array(countOfFiveMinuteSlices || 1)
      .fill('☕️')
      .join('')} ${minutes} min read`;

  return `${new Array(Math.round(countOfFiveMinuteSlices / Math.E))
    .fill('🍱')
    .join('')} ${minutes} min read`;
}

// `lang` is optional and will default to the current user agent locale
export function formatPostDate(date: Date, lang?: string) {
  if (typeof Date.prototype.toLocaleDateString !== 'function') {
    return `${date.getFullYear()}-${date.getMonth()}-${date.getDate()}`;
  }

  return new Date(date).toLocaleDateString(lang, {
    day: 'numeric',
    month: 'long',
    year: 'numeric',
  });
}

export function addHours(date: Date, hours: number) {
  const newDate = new Date(date);
  newDate.setDate(date.getHours() + hours);
  return newDate;
}

export function getRandomInt(min: number, max: number) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

export function truncateText(text: string, maxLength: number = 80) {
  return text.length > 80 ? text.slice(0, 80).concat('...') : text;
}

export const descDateSorter = (a: Date, b: Date) => b.getTime() - a.getTime();
