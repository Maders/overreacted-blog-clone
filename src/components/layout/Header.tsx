import React, { CSSProperties, useState } from 'react';
import { useRouter } from 'next/router';
import Link from 'next/link';

import {
  selector,
  toggle,
  useThemeToggleSelector,
  useThemeToggleDispatch,
} from 'src/modules/ThemeToggle';
import { ToggleButton } from 'src/components/toggle-button';
import { websiteConfigs } from 'src/core/siteConfig';

import styles from './Layout.module.css';

const runtimeStyles: Record<string, CSSProperties> = {
  headerNotRootPath: {
    fontFamily: 'Montserrat, sans-serif',
    height: 42,
  },
  titleNotRootPath: {
    color: 'var(--pink)',
  },
};

export function Header() {
  const router = useRouter();
  const isNotRootPath = router.asPath !== '/';
  const headerInlineStyle = isNotRootPath
    ? runtimeStyles.headerNotRootPath
    : undefined;
  const titleInlineStyle = isNotRootPath
    ? runtimeStyles.titleNotRootPath
    : undefined;

  const isDarkMode = useThemeToggleSelector(selector.isDarkMode);
  const dispatch = useThemeToggleDispatch();
  return (
    <header className={styles.header} style={headerInlineStyle}>
      <Link href={'/'}>
        <a className={styles['title-link']} style={titleInlineStyle}>
          <h1>{websiteConfigs.metadata.title}</h1>
        </a>
      </Link>
      <ToggleButton
        defaultChecked={isDarkMode}
        onChange={(_) => {
          dispatch(toggle());
        }}
      />
    </header>
  );
}
