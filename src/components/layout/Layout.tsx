import React, { Fragment, PropsWithChildren } from 'react';
import Head from 'next/head';

import { useThemeToggleSelector, selector } from 'src/modules/ThemeToggle';

import { Header } from './Header';
import styles from './Layout.module.css';

const THEME_BG = {
  light: {
    bg: '#ffa8c5',
  },
  dark: {
    bg: '#282c35',
  },
};

export function Layout({ children }: PropsWithChildren<{}>) {
  const mode = useThemeToggleSelector(selector.mode);

  return (
    <Fragment>
      <Head>
        <meta
          name="theme-color"
          content={mode === 'light' ? THEME_BG.light.bg : THEME_BG.dark.bg}
        />
      </Head>
      <div id="body" className={mode}>
        <div className={styles.container}>
          <div className={styles.layout}>
            <Header />
            {children}
          </div>
        </div>
      </div>
    </Fragment>
  );
}
