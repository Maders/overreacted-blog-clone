import Image from 'next/image';
import React, {
  FocusEvent,
  HTMLProps,
  MouseEvent,
  TouchEvent,
  useRef,
  useState,
} from 'react';
import moon from './assets/moon.png';
import sun from './assets/sun.png';
import styles from './ToggleButton.module.css';

export function pointerCoord<T>(event: TouchEvent<T> | undefined) {
  // get coordinates for either a mouse click
  // or a touch depending on the given event
  if (event) {
    const changedTouches = event.changedTouches;
    if (changedTouches && changedTouches.length > 0) {
      const touch = changedTouches[0];
      return { x: touch.clientX, y: touch.clientY };
    }
  }
  return { x: 0, y: 0 };
}

interface ToggleButtonProps extends HTMLProps<HTMLInputElement> {
  checked?: boolean;
  disabled?: boolean;
  defaultChecked?: boolean;
  className?: string;
  checkedIcon?: React.ReactNode;
  uncheckedIcon?: React.ReactNode;
}

export function ToggleButton(props: ToggleButtonProps) {
  const { defaultChecked = false, checked: checkedProps } = props;
  const [focus, setFocus] = useState(false);
  const [checked, setChecked] = useState(checkedProps || defaultChecked);
  const checkboxInputRef = useRef<null | HTMLInputElement>(null);
  const moved = useRef<boolean>(false);
  const startX = useRef<null | number>(null);
  const activated = useRef(false);
  const previouslyChecked = useRef(checkedProps || defaultChecked);
  const classnames =
    styles['react-toggle'] +
    (checked ? ` ${styles['react-toggle--checked']}` : '') +
    (focus ? ` ${styles['react-toggle--focus']}` : '') +
    (props.disabled ? ` ${styles['react-toggle--disabled']}` : '');

  function handleClick(event: MouseEvent<HTMLInputElement>) {
    if (props.disabled) return;

    if (!Object.is(event.target, checkboxInputRef.current) && !moved.current) {
      previouslyChecked.current = checkboxInputRef.current?.checked || false;
      event.preventDefault();
      checkboxInputRef.current?.focus();
      checkboxInputRef.current?.click();
      return;
    }

    const checked = props.checked ?? checkboxInputRef.current?.checked!;

    setChecked(checked);
  }

  function handleTouchStart(event: TouchEvent<HTMLDivElement>) {
    if (props.disabled) return;

    startX.current = pointerCoord(event).x;
    activated.current = true;
  }

  function handleTouchMove(event: TouchEvent<HTMLDivElement>) {
    if (!activated.current) return;

    moved.current = true;

    if (startX.current) {
      let currentX = pointerCoord(event).x;
      if (checked && currentX + 15 < startX.current) {
        setChecked(false);
        startX.current = currentX;
        activated.current = true;
      } else if (currentX - 15 > startX.current) {
        setChecked(true);
        startX.current = currentX;
        activated.current = currentX < startX.current + 5;
      }
    }
  }

  function handleTouchEnd(event: TouchEvent<HTMLDivElement>) {
    if (!moved.current) return;
    event?.preventDefault();

    if (startX.current) {
      let endX = pointerCoord(event).x;
      if (previouslyChecked.current === true && startX.current + 4 > endX) {
        if (previouslyChecked.current !== checked) {
          setChecked(false);
          previouslyChecked.current = checked;
          checkboxInputRef.current?.click();
        }
      } else if (startX.current - 4 < endX) {
        if (previouslyChecked.current !== checked) {
          setChecked(true);
          previouslyChecked.current = checked;
          checkboxInputRef.current?.click();
        }
      }

      activated.current = false;
      startX.current = null;
      moved.current = false;
    }
  }

  function handleFocus(event: FocusEvent<HTMLInputElement>) {
    const { onFocus } = props;

    if (onFocus) {
      onFocus(event);
    }

    setFocus(true);
  }

  function handleBlur(event: FocusEvent<HTMLInputElement>) {
    const { onBlur } = props;

    if (onBlur) {
      onBlur(event);
    }

    setFocus(false);
  }

  const {
    className,
    checkedIcon,
    uncheckedIcon,
    checked: _,
    ...inputProps
  } = props;
  return (
    <div
      className={classnames}
      onClick={handleClick}
      onTouchStart={handleTouchStart}
      onTouchMove={handleTouchMove}
      onTouchEnd={handleTouchEnd}
    >
      <div className={styles['react-toggle-track']}>
        <div className={styles['react-toggle-track-check']}>
          {uncheckedIcon ? (
            uncheckedIcon
          ) : (
            <Image
              alt="dark mode"
              src={moon}
              width="16"
              height="16"
              role="presentation"
              style={{ pointerEvents: 'none' }}
            />
          )}
        </div>
        <div className={styles['react-toggle-track-x']}>
          {checkedIcon ? (
            checkedIcon
          ) : (
            <Image
              alt="light mode"
              src={sun}
              width="16"
              height="16"
              role="presentation"
              style={{ pointerEvents: 'none' }}
            />
          )}
        </div>
      </div>
      <div className={styles['react-toggle-thumb']} />

      <input
        {...inputProps}
        ref={checkboxInputRef}
        onFocus={handleFocus}
        onBlur={handleBlur}
        className={styles['react-toggle-screenreader-only']}
        type="checkbox"
        aria-label="Switch between Dark and Light mode"
      />
    </div>
  );
}
