import React from 'react';
import Image from 'next/image';

import ProfilePic from './assets/profile-pic.jpg';

export function AuthorInfo() {
  return (
    <div
      style={{
        display: 'flex',
      }}
    >
      <Image
        src={ProfilePic}
        height="55.99px"
        width="55.99px"
        alt="Dan Abramov"
        layout="fixed"
        style={{
          marginBottom: 0,
          width: '3.5rem',
          height: '3.5rem',
          borderRadius: '50%',
        }}
      />
      <p style={{ maxWidth: 310, marginLeft: '0.875rem' }}>
        Personal blog by{' '}
        <a href="https://mobile.twitter.com/dan_abramov">Dan Abramov</a>.{' '}
        I&nbsp;explain with words and code.
      </p>
    </div>
  );
}
