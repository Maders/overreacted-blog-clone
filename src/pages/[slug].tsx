import { Fragment } from 'react';
import type { GetServerSideProps, NextPage } from 'next';
import slugify from 'slugify';

import { serverPostsFetcher } from 'src/modules/posts';
import { PostPage, PostPageProps } from 'src/modules/posts/PostPage';
import { SEO } from 'src/core/meta-tags/SEO';

interface HomePageProps {
  post: PostPageProps;
}

const Post: NextPage<HomePageProps> = ({ post }) => {
  return (
    <Fragment>
      <SEO title={post.title} description={post.body} />
      <PostPage {...post} />
    </Fragment>
  );
};

export const getServerSideProps: GetServerSideProps<HomePageProps> = async ({
  query,
}) => {
  const postSlug = query.slug;

  let posts = null;
  try {
    posts = await serverPostsFetcher();
  } catch (e) {}

  let foundedPost = posts?.find((post) => postSlug === slugify(post.title));

  if (!foundedPost || !posts) {
    return {
      notFound: true,
    };
  }

  return {
    props: {
      post: foundedPost,
    },
  };
};

export default Post;
