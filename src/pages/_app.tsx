import '../styles/globals.css';
import type { AppProps } from 'next/app';
import { Layout } from 'src/components/layout';
import { Provider } from 'react-redux';
import { store } from 'src/modules/ThemeToggle';

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <Provider store={store}>
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </Provider>
  );
}

export default MyApp;
