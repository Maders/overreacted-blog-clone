import { Fragment } from 'react';
import type { GetServerSideProps, NextPage } from 'next';

import { serverPostsFetcher } from 'src/modules/posts';
import { HomePage, HomePageProps } from 'src/modules/posts/HomePage';
import { SEO } from 'src/core/meta-tags/SEO';

const Home: NextPage<HomePageProps> = ({ posts }) => {
  return (
    <Fragment>
      <SEO />
      <HomePage posts={posts} />
    </Fragment>
  );
};

export const getServerSideProps: GetServerSideProps<
  HomePageProps
> = async () => {
  let posts = null;
  try {
    posts = await serverPostsFetcher();
  } catch (e) {}

  if (!posts) {
    return {
      notFound: true,
    };
  }

  return {
    props: {
      posts,
    },
  };
};

export default Home;
