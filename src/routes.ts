import { API_BASE_URL } from './core/envs';

export const apis = {
  posts: `${API_BASE_URL}/posts`,
};
