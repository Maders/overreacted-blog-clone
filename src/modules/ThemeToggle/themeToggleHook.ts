import { TypedUseSelectorHook, useDispatch, useSelector } from 'react-redux';
import type { RootState, AppDispatch } from './store';

export const useThemeToggleDispatch = () => useDispatch<AppDispatch>();
export const useThemeToggleSelector: TypedUseSelectorHook<RootState> = useSelector;
