import { createSlice, configureStore } from '@reduxjs/toolkit';

const counterSlice = createSlice({
  name: 'themeMode',
  initialState: {
    mode: 'light',
  },
  reducers: {
    toggle: (state) => {
      let finalState = null;
      if (state.mode === 'light') finalState = 'dark';
      if (state.mode === 'dark') finalState = 'light';

      state.mode = finalState!;
    },
  },
});

export const { toggle } = counterSlice.actions;


function makeStore() {
  return configureStore({
    reducer: counterSlice.reducer,
  });
}


export const store = makeStore();
 
export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;

const isDarkMode = (state: RootState) => state.mode === 'dark'
const isLightMode = (state: RootState) => state.mode === 'dark'
const mode = (state: RootState) => state.mode;

export const selector = {
  isDarkMode, isLightMode, mode
}