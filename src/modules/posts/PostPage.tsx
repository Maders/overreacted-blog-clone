import React from 'react';

import { Post } from 'src/core/types';
import { formatPostDate } from 'src/utils';
import styles from './Home.module.css';

export type PostPageProps = Omit<Omit<Post, 'timeToRead'>, 'spoiler'>;

export function PostPage({ body, createdAt, id, title }: PostPageProps) {
  return (
    <main className={styles.main}>
      <div className={styles.grid}>
        <article key={id}>
          <header>
            <h1 style={{ color: 'var(--textTitle)' }}>{title}</h1>
            <p
              style={{
                fontSize: '0.83255rem',
                lineHeight: '1.75rem',
                display: 'block',
                marginBottom: '1.75rem',
                marginTop: '-1.4rem',
              }}
            >
              {formatPostDate(new Date(createdAt))}
            </p>
          </header>

          <p>{body}</p>
        </article>
      </div>
    </main>
  );
}
