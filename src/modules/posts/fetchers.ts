import serverFetch from 'node-fetch';
import { ApiPost, Post } from 'src/core/types';
import { apis } from 'src/routes';
import {
  addHours,
  descDateSorter,
  getRandomInt,
  truncateText,
} from 'src/utils';
import { BASE_DATE } from './constants';

export const serverPostsFetcher = () =>
  new Promise<Post[]>(async (resolve, reject) => {
    try {
      const result = (await (
        await serverFetch(apis.posts)
      ).json()) as ApiPost[];

      const initialDate = new Date(BASE_DATE);
      const postwithDates = result
        .map((post) => {
          const createdAt = addHours(initialDate, post.id);
          const timeToRead = getRandomInt(1, 60);
          return {
            ...post,
            spoiler: truncateText(post.body, 60),
            createdAt,
            timeToRead,
          };
        })
        .sort(({ createdAt: a }, { createdAt: b }) => descDateSorter(a, b))
        .map((_) => ({ ..._, createdAt: _.createdAt.toISOString() }));

      resolve(postwithDates);
    } catch (error) {
      reject(error);
    }
  });
