import React, { Fragment } from 'react';
import Link from 'next/link';
import slugify from 'slugify';

import {
  formatPostDate,
  formatReadingTime,
} from 'src/utils';
import { AuthorInfo } from 'src/components/author-Info';
import { Post } from 'src/core/types';

import styles from './Home.module.css';

export type HomePageProps = {
  posts: Post[];
};

export function HomePage({ posts }: HomePageProps) {
  return (
    <Fragment>
      <AuthorInfo />
      <main className={styles.main}>
        <div className={styles.grid}>
          {posts.map((post) => {
            return (
              <article key={post.id}>
                <header>
                  <h3 className={styles['post-title']}>
                    <Link rel="bookmark" href={`/${slugify(post.title)}`}>
                      <a>{post.title}</a>
                    </Link>
                  </h3>
                </header>
                <small>{formatPostDate(new Date(post.createdAt))}</small>
                <small>{` • ${formatReadingTime(post.timeToRead)}`}</small>

                <p>{post.spoiler}</p>
              </article>
            );
          })}
        </div>
      </main>
    </Fragment>
  );
}
