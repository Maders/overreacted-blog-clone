This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First install project dependencies with `yarn`, change to root directory of project and run:
```bash
yarn 
# or 
yarn install
```

Second copy `.env.production` file and paste it with name of `.env` and then run the development server:
```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.


for production build run:
```bash
npm run build
# or
yarn build
```

## Done:
1. Index page
2. seo tags
3. toggle button
4. Post page
5. dark/light theme toggling
6. Next.js with SSR
7. Typescript
8. descending sort

### Notes and Improvement:
- serialize redux theme store in server-side response and re-heydrating in rendering phase
- consistent styling and theme based on `styled-system` APIs
* server side logis can be more abstracted but in current steps is enough and reasonable
